Dear CRAN Team,
this is a resubmission of package 'excerptr'. I have added the following changes:

* Removed LazyData from file DESCRIPTION.

Please upload to CRAN.
Best, Andreas Dominik

# Package excerptr 2.0.1

Reporting is done by packager version 1.13.0


## Test environments
- R Under development (unstable) (2021-07-26 r80665)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 3 (beowulf)
   0 errors | 0 warnings | 0 notes
- gitlab.com
  R version 4.1.0 (2021-05-18)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 20.04.2 LTS
  Package: excerptr
  Version: 2.0.1
  Status: OK

- win-builder (devel)

## Local test results
- RUnit:
    excerptr_unit_test - 1 test function, 0 errors, 0 failures in 1 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 3 ]
- Coverage by covr:
    excerptr Coverage: 96.43%

## Local meta results
- Cyclocomp:
     no issues.
- lintr:
    found 1 lints in 97 lines of code (a ratio of 0.0103).
- cleanr:
    found 0 dreadful things about your code.
- codetools::checkUsagePackage:
    found 2 issues.
- devtools::spell_check:
    found 14 unkown words.
